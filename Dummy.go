package log

import (
	"time"

	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// Dummy is a dumb implementation of Logger that does absolutely nothing.
type Dummy struct{}

var _ Logger = &Dummy{}

// NewDummy creates a new dummy logger.
func NewDummy() *Dummy {
	return &Dummy{}
}

// Trace does nothing.
func (d *Dummy) Trace(model trace.Traceable) error {
	return nil
}

// BeginRequest does nothing.
func (d *Dummy) BeginRequest(correlationID id.ID, method string, path string, ip string) error {
	return nil
}

// EndRequest does nothing.
func (d *Dummy) EndRequest(correlationID id.ID, code int, bytesWritten int64, time time.Duration) error {
	return nil
}

// Info does nothing.
func (d *Dummy) Info(correlationID id.ID, title string, message string) error {
	return nil
}

// Warning does nothing.
func (d *Dummy) Warning(correlationID id.ID, title string, message string) error {
	return nil
}

// Error does nothing.
func (d *Dummy) Error(correlationID id.ID, title string, message string) error {
	return nil
}

// Panic does nothing.
func (d *Dummy) Panic(correlationID id.ID, x interface{}) error {
	return nil
}
