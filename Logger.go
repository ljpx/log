package log

import (
	"time"

	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// Logger defines a generic set of logging methods for general use.
type Logger interface {
	Trace(model trace.Traceable) error

	BeginRequest(correlationID id.ID, method string, path string, ip string) error
	EndRequest(correlationID id.ID, code int, bytesWritten int64, time time.Duration) error

	Info(correlationID id.ID, title string, message string) error
	Warning(correlationID id.ID, title string, message string) error
	Error(correlationID id.ID, title string, message string) error

	Panic(correlationID id.ID, x interface{}) error
}
