package log

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestDummyShouldAlwaysReturnNilError(t *testing.T) {
	// Arrange.
	dummy := NewDummy()

	// Act and Assert.
	require.Nil(t, dummy.Trace(nil))
	require.Nil(t, dummy.BeginRequest(id.New(), "", "", ""))
	require.Nil(t, dummy.EndRequest(id.New(), 0, 0, 0))
	require.Nil(t, dummy.Info(id.New(), "", ""))
	require.Nil(t, dummy.Warning(id.New(), "", ""))
	require.Nil(t, dummy.Error(id.New(), "", ""))
	require.Nil(t, dummy.Panic(id.New(), nil))
}
